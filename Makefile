# -*- Mode: BSDmakefile; -*-                                          
#
# Makefile for Rosie Pattern Language
#
# © Copyright Jamie A. Jennings 2019, 2020.
# © Copyright IBM Corporation 2018.
# LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
# AUTHOR: Jamie A. Jennings
#
#
# 'make' builds rosie in a local directory called 'build'.
#
# 'make install' copies the contents of the build directory to a
#   specified destination.  The default is /usr/local.
#
# 'make test' runs all of the blackbox and whitebox tests.  For the
#   tests to execute, rosie must have been built with LUADEBUG=1.
#
# Additional command-line options for 'make':
#   LUADEBUG=1 enables access (via Lua) to internals for testing
#   DEBUG=1    enables assertions
#   LOGGING=1  enables logging
#   ASAN=1     enables address sanitizing (large performance hit)


# -----------------------------------------------------------------------------
# First, some recommended hygiene for Makefiles, to help catch errors:
SHELL := bash
.SHELLFLAGS := -e -o pipefail -c
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Customizable options
# -----------------------------------------------------------------------------

DESTDIR ?= /usr/local

# -----------------------------------------------------------------------------
# Useful targets for packagers
# -----------------------------------------------------------------------------

# make fetch
#  Clones the needed git submodules and creates a needed symlink
#
# make compile
#  Builds librosie (in static and dynamic flavors) and the rosie executable.
#  The entire 'build' directory is populated, in preparation for later install.
#
# make check
#  Attempts to run 'build/bin/rosie version' and ensures output is correct.
#

# -----------------------------------------------------------------------------
# Build with CLI support (The default for the top-level Makefile)
# -----------------------------------------------------------------------------

export ROSIE_CLI = 1

# -----------------------------------------------------------------------------
# Enable address santizing when desired
# -----------------------------------------------------------------------------

ifdef ASAN
export ASAN_OPT = -fsanitize=address,undefined
endif

# -----------------------------------------------------------------------------
# Platform detection
# -----------------------------------------------------------------------------

REPORTED_PLATFORM=$(shell (uname -o || uname -s) 2> /dev/null)
ifeq ($(REPORTED_PLATFORM), Darwin)
PLATFORM=macosx
else ifeq ($(REPORTED_PLATFORM), GNU/Linux)
PLATFORM=linux
else ifeq ($(REPORTED_PLATFORM), Linux)
PLATFORM=linux
else
$(error Unsupported platform (uname reported "$(REPORTED_PLATFORM)"))
endif

# -----------------------------------------------------------------------------
# The BUILD_ROOT is the directory where all needed files have been downloaded
# -----------------------------------------------------------------------------

BUILD_ROOT = $(shell pwd)

BUILD = $(BUILD_ROOT)/build
ROSIEBIN = $(BUILD)/bin/rosie
ROSIEBINSCRIPTNAME = rosie
ROSIEBINSCRIPT = $(BUILD_ROOT)/$(ROSIEBINSCRIPTNAME)
ROSIE ?= $(ROSIEBINSCRIPT)

RPEG_DIR = $(BUILD_ROOT)/src/rpeg
LIBROSIE_DIR = $(BUILD_ROOT)/src/librosie
SUBMOD_DIR = $(BUILD_ROOT)/submodules

# File names for the librosie variants
LIBROSIE_H=librosie.h
ifeq ($(PLATFORM),macosx)
PLATFORM=macosx
CC=cc
LIBROSIE_DYLIB=librosie.dylib
else ifeq ($(PLATFORM),linux)
PLATFORM=linux
CC=gcc
LIBROSIE_DYLIB=librosie.so
endif

# -----------------------------------------------------------------------------
# Submodules (which, alas, are not a well-designed feature of git)
# -----------------------------------------------------------------------------

LUA_DIR = $(SUBMOD_DIR)/$(LUA)
JSON_DIR = $(SUBMOD_DIR)/$(JSON)
LUAMOD_DIR = $(SUBMOD_DIR)/$(LUAMOD)
READLINE_DIR = $(SUBMOD_DIR)/$(READLINE)

# Submodules
ARGPARSE = argparse
LUA = lua
JSON = lua-cjson
READLINE = lua-readline
LUAMOD = lua-modules

# Lua compiler used during build
LUACBIN = $(LUA_DIR)/src/luac

# Output of luac goes into build dir, here:
LUACD = $(BUILD)/lib/rosie/lib

# -----------------------------------------------------------------------------
# Install layout
# -----------------------------------------------------------------------------

LIBROSIED = $(DESTDIR)/lib
ROSIED = $(DESTDIR)/lib/rosie
INSTALL_MAN_DIR = $(DESTDIR)/share/man/man1
INSTALL_BIN_DIR = $(DESTDIR)/bin
INSTALL_INCLUDE_DIR = $(DESTDIR)/include

# Almost everything gets copied to $(ROSIED): 
#   $(ROSIED)/bin          arch-dependent binaries (e.g. rosie)
#   $(ROSIED)/lib          arch-dependent libraries (e.g. *.luac)
#   $(ROSIED)/rpl          standard library (*.rpl)
#   $(ROSIED)/doc          documentation (html format)
#   $(ROSIED)/extra        editor highlighting files, sample docker files, other things
#   $(ROSIED)/CHANGELOG    change log
#   $(ROSIED)/CONTRIBUTORS project contributors, acknowledgements
#   $(ROSIED)/LICENSE      license
#   $(ROSIED)/README       short text readme (e.g. where to open issues)
#   $(ROSIED)/VERSION      installed version
#
# Rosie executable goes here:
#   $(DESTDIR)/bin/rosie
# 
# Man page goes here:
#   $(DESTDIR)/share/man/man1

INSTALL_LIB_DIR = $(ROSIED)/lib
INSTALL_RPL_DIR = $(ROSIED)/rpl
INSTALL_DOC_DIR = $(ROSIED)/doc
INSTALL_EXTRA_DIR = $(ROSIED)/extra
INSTALL_ROSIEBIN = $(INSTALL_BIN_DIR)/rosie

# -----------------------------------------------------------------------------
# Targets
# -----------------------------------------------------------------------------

.PHONY:
default:
	$(MAKE) fetch
	$(MAKE) compile
	$(MAKE) copy_to_build
	$(MAKE) check

RPL_FILES = $(shell find $(BUILD_ROOT)/rpl \! -name '*~' -and \! -name '.DS_Store' -and -type f)
RPL_FILES_BUILD = $(patsubst $(BUILD_ROOT)/%,$(BUILD)/lib/rosie/%,$(RPL_FILES))

DOC_FILES = $(shell find $(BUILD_ROOT)/doc \! -name '*~' -and \! -name '.DS_Store' -and -type f)
DOC_FILES_BUILD = $(patsubst $(BUILD_ROOT)/%,$(BUILD)/%,$(DOC_FILES))

EXTRA_FILES = $(shell find $(BUILD_ROOT)/extra \! -name '*~' -and \! -name '.DS_Store' -and -type f)
EXTRA_FILES_BUILD = $(patsubst $(BUILD_ROOT)/%,$(BUILD)/lib/rosie/%,$(EXTRA_FILES))

.PHONY: copy_to_build 
copy_to_build: builddir $(BUILD)/include/$(LIBROSIE_H) $(RPL_FILES_BUILD) $(DOC_FILES_BUILD) $(EXTRA_FILES_BUILD)

$(BUILD)/include/$(LIBROSIE_H): $(BUILD_ROOT)/src/librosie/$(LIBROSIE_H)
	@mkdir -p $(shell dirname "$@")
	cp "$<" "$@"

$(BUILD)/lib/rosie/rpl/%: $(BUILD_ROOT)/rpl/%
	@mkdir -p $(shell dirname "$@")
	cp "$<" "$@"

$(BUILD)/doc/%: $(BUILD_ROOT)/doc/%
	@mkdir -p $(shell dirname "$@")
	cp "$<" "$@"

$(BUILD)/lib/rosie/extra/%: $(BUILD_ROOT)/extra/%
	@mkdir -p $(shell dirname "$@")
	cp "$<" "$@"

.PHONY:
compile: $(ROSIEBIN)

$(ROSIEBIN): $(LIBROSIE_DIR)/binaries/rosie VERSION
	cp $(BUILD_ROOT)/src/librosie/binaries/$(LIBROSIE_DYLIB) $(BUILD)/lib/$(LIBROSIE_DYLIB)
	cp -R $(BUILD_ROOT)/doc $(BUILD)
	cp -R $(BUILD_ROOT)/extra $(BUILD)/lib/rosie
	cd $(BUILD_ROOT); cp CHANGELOG CONTRIBUTORS LICENSE README VERSION $(BUILD)/lib/rosie
	cp $(LIBROSIE_DIR)/binaries/rosie $(BUILD)/bin/rosie

# <sigh> Once we have packages to build rosie (e.g. rpm, nix), we
# won't need this test.  Note that this test should ALWAYS pass on
# MacOS, since it ships with readline.
.PHONY: readlinetest
readlinetest: cctest
	@(bash -c 'printf "#include <stdio.h>\n#include <readline/readline.h>\nint main() { }\n"' | \
	           $(CC) -std=c99 -lreadline -o /dev/null -xc -) && \
	   echo 'libreadline and readline.h appear to be installed' || \
	   (echo '**'; echo '** readline TEST: Missing readline library or readline.h'; echo '**' && false)

.PHONY: cctest
cctest:
	@(bash -c 'printf "int main() { }\n"' | \
		$(CC) -std=c99 -o  /dev/null -xc -) && \
	echo "C compiler $(CC) appears to be working" || \
	(echo '**'; \
	echo '** C compiler test FAILED. (If on macos, please install XCode Command Line Tools.)'; \
	echo '**' && false)

# When we compile with gcc, such as on linux, we need bsd/stdio.h to
# compile rpeg.  We check for it here so that we can give a useful
# error message.
.PHONY: libbsdtest
libbsdtest: cctest
	@(bash -c 'printf "#if defined(__linux__)\n#if !defined(__clang__)\n#include <bsd/stdio.h>\n#endif\n#endif\nint main() { }\n"' | \
	           $(CC) -std=c99 -o /dev/null -xc -) && \
	   echo 'bsd/stdio.h appears to be installed (or is not needed)' || \
	   (echo '**'; echo '** libbsd TEST: Missing bsd/stdio.h.  Try running: apt-get install libbsd'; echo '**' && false)

# The submodule_sentinel indicates that submodules have been
# initialized in the git sense, i.e. that they have been cloned.  The
# sentile file is a file copied from a submodule repo, so that:
# (1) the submodule must have been checked out, and
# (2) the sentinel will not be newer than the submodule files
submodule_sentinel=submodules/~~present~~ 
submodules = submodules/lua/src/Makefile \
		submodules/lua-cjson/Makefile \
		submodules/lua-readline/Makefile
$(submodules): $(submodule_sentinel)

.PHONY:
fetch: $(submodule_sentinel)

$(submodule_sentinel): 
	if [ -z $$BREW ]; then git submodule update --init --depth 1; fi
	cd $(LUA_DIR) && rm -f include && ln -sf src include
	cp -p $(LUA_DIR)/README $(submodule_sentinel)
	@$(BUILD_ROOT)/src/build_info.sh "git_submodules" $(BUILD_ROOT) "git" >> $(BUILD_ROOT)/build.log

$(LUACBIN) $(LUA_DIR)/src/lua: $(submodules)
	$(MAKE) -C "$(LUA_DIR)" CC=$(CC) $(PLATFORM)
	@$(BUILD_ROOT)/src/build_info.sh "lua" $(BUILD_ROOT) $(CC) >> $(BUILD_ROOT)/build.log


## ----------------------------------------------------------------------------------------

$(LUACD)/argparse.luac: $(submodules) submodules/argparse/src/argparse.lua $(LUACBIN) | builddir
	$(LUACBIN) -o $(LUACD)/argparse.luac submodules/argparse/src/argparse.lua
	@$(BUILD_ROOT)/src/build_info.sh "argparse" $(BUILD_ROOT) "$(LUACBIN)" >> $(BUILD_ROOT)/build.log

readline_lib = $(READLINE_DIR)/readline.so
$(LUACD)/readline.so: $(readline_lib) 

$(READLINE_DIR)/readline.so: $(submodules) 
	$(MAKE) -C "$(READLINE_DIR)" CC=$(CC) LUADIR=../$(LUA)
	@$(BUILD_ROOT)/src/build_info.sh "readline_stub" $(BUILD_ROOT) $(CC) >> $(BUILD_ROOT)/build.log

$(LUACD)/strict.luac: $(LUAMOD_DIR)/strict.lua $(LUACBIN) | builddir
	$(LUACBIN) -o $@ $<

$(LUACD)/list.luac: $(LUAMOD_DIR)/list.lua $(LUACBIN) | builddir
	$(LUACBIN) -o $@ $<

$(LUACD)/thread.luac: $(LUAMOD_DIR)/thread.lua $(LUACBIN) | builddir
	$(LUACBIN) -o $@ $<

$(LUACD)/recordtype.luac: $(LUAMOD_DIR)/recordtype.lua $(LUACBIN) | builddir
	$(LUACBIN) -o $@ $<

$(LUACD)/submodule.luac: $(LUAMOD_DIR)/submodule.lua $(LUACBIN) | builddir
	$(LUACBIN) -o $@ $<

core_sources = $(wildcard $(BUILD_ROOT)/src/lua/*.lua)
core_objects = $(patsubst $(BUILD_ROOT)/src/lua/%.lua,$(LUACD)/%.luac,$(core_sources))

$(LUACD)/%.luac: $(BUILD_ROOT)/src/lua/%.lua $(LUACBIN) | builddir
	$(LUACBIN) -o $@ $<
	@$(BUILD_ROOT)/src/build_info.sh $@ $(BUILD_ROOT) "$(LUACBIN)" >> $(BUILD_ROOT)/build.log

lua_objects: $(core_objects) $(LUACD)/argparse.luac $(LUACD)/list.luac $(LUACD)/recordtype.luac \
		$(LUACD)/submodule.luac $(LUACD)/strict.luac $(LUACD)/thread.luac


.PHONY:
builddir:
	@if [ ! -d $(BUILD) ]; then \
	mkdir -p $(BUILD); \
	mkdir -p $(BUILD)/bin; \
	mkdir -p $(BUILD)/lib; \
	mkdir -p $(BUILD)/lib/rosie; \
	mkdir -p $(BUILD)/lib/rosie/lib; \
	mkdir -p $(BUILD)/include; \
	mkdir -p $(BUILD)/doc/man; \
	fi


binaries = $(LIBROSIE_DIR)/binaries/rosie
$(binaries): lua_objects $(readline_lib) VERSION
	@echo "Checking for key dependencies..."
	$(MAKE) libbsdtest readlinetest
	@echo "Building librosie and other binaries..."
	@$(MAKE) -C $(LIBROSIE_DIR) 
	@$(BUILD_ROOT)/src/build_info.sh "binaries" $(BUILD_ROOT) $(CC) >> $(BUILD_ROOT)/build.log




# -----------------------------------------------------------------------------
# Install
# -----------------------------------------------------------------------------

# Main install rule
.PHONY: install installforce 
install:
	@if [ -L "$(ROSIED)" ]; then \
		echo "$(ROSIED) exists and is a symbolic link."; \
		echo "If rosie was installed with 'brew', then run 'brew uninstall rosie'."; \
		echo "Or run 'make installforce' to overwrite the current installation."; \
		exit -1; \
	elif [ -e "$(ROSIED)" ]; then \
		echo "$(ROSIED) already exists. Run 'make installforce' to overwrite it."; \
		exit -1; \
	elif [ -e "$(DESTDIR)/bin/rosie" ]; then \
		echo "$(DESTDIR)/bin/rosie already exists. Run 'make installforce' to overwrite it."; \
		exit -1; \
	else \
		echo "Installing..."; \
		$(MAKE) installforce; \
	fi;

installforce: | check
# In case the destination directory (e.g. /usr/local/lib/rosie) is a symlink, we try to rm it first.
	-rm -f "$(ROSIED)"
	mkdir -p "$(ROSIED)"
	cp -R $(BUILD)/lib/rosie/* "$(ROSIED)"
	-rm -f "$(LIBROSIED)/$(LIBROSIE_DYLIB)"
	cp "$(BUILD)/lib/$(LIBROSIE_DYLIB)" "$(LIBROSIED)/$(LIBROSIE_DYLIB)"
	mkdir -p "$(INSTALL_INCLUDE_DIR)"
	cp "$(BUILD)/include/$(LIBROSIE_H)" "$(INSTALL_INCLUDE_DIR)/$(LIBROSIE_H)"
	mkdir -p "$(INSTALL_DOC_DIR)"
	cp -R $(BUILD)/doc/ "$(INSTALL_DOC_DIR)"
	mkdir -p "$(INSTALL_BIN_DIR)"
	-rm -f "$(INSTALL_ROSIEBIN)"
	cp $(BUILD)/bin/rosie "$(INSTALL_ROSIEBIN)"
	-rm -f "$(INSTALL_MAN_DIR)/rosie.1"
	mkdir -p "$(INSTALL_MAN_DIR)"
	cp $(BUILD)/doc/man/rosie.1 "$(INSTALL_MAN_DIR)/rosie.1"
	if [ "$(PLATFORM)" == "linux" ]; then \
		echo "$(LIBROSIED)" > /etc/ld.so.conf.d/librosie.conf; \
		ldconfig; \
	fi
	@echo "Testing installation with 'make installcheck"
	@if [ -z "$$BREW" ]; then \
		$(MAKE) installcheck; \
	fi

# -----------------------------------------------------------------------------
# Uninstall
# -----------------------------------------------------------------------------

.PHONY: uninstall
uninstall:
	@if [ -e "$(INSTALL_ROSIEBIN)" ]; then \
		echo "Removing $(INSTALL_ROSIEBIN)"; \
		rm -f $(INSTALL_ROSIEBIN); \
	else echo "$(INSTALL_ROSIEBIN) not found"; \
	fi
	@if [ -e "$(ROSIED)" ]; then \
		echo "Removing $(ROSIED)"; \
		rm -Rf $(ROSIED)/; \
	else echo "$(ROSIED) not found"; \
	fi
	@if [ -e "$(LIBROSIED)/$(LIBROSIE_DYLIB)" ]; then \
		echo "Removing $(LIBROSIED)/$(LIBROSIE_DYLIB)"; \
		rm -f "$(LIBROSIED)/$(LIBROSIE_DYLIB)"; \
	else echo "$(LIBROSIED)/$(LIBROSIE_DYLIB) not found"; \
	fi
	@if [ -e "$(INSTALL_INCLUDE_DIR)/$(LIBROSIE_H)" ]; then \
		echo "Removing $(INSTALL_INCLUDE_DIR)/$(LIBROSIE_H)"; \
		rm -f "$(INSTALL_INCLUDE_DIR)/$(LIBROSIE_H)"; \
	else echo "$(INSTALL_INCLUDE_DIR)/$(LIBROSIE_H) not found"; \
	fi
	@if [ -e "$(INSTALL_MAN_DIR)/rosie.1" ]; then \
		echo "Removing $(INSTALL_MAN_DIR)/rosie.1"; \
		rm -f "$(INSTALL_MAN_DIR)/rosie.1"; \
	else echo "$(INSTALL_MAN_DIR)/rosie.1 not found"; \
	fi
	@if [ "$(PLATFORM)" == "linux" ]; then \
		if [ -e "/etc/ld.so.conf.d/librosie.conf" ]; then \
			echo "Removing /etc/ld.so.conf.d/librosie.conf"; \
			rm -f "/etc/ld.so.conf.d/librosie.conf"; \
		fi; \
	fi

.PHONY: check
check: 
	@if [ ! -x $(ROSIEBIN) ]; then \
	echo "$(ROSIEBIN) does not exist.  Run 'make' to build rosie."; \
	false; \
	fi 
	@echo ""
	@echo "Checking the build using ./$(ROSIEBINSCRIPTNAME)"
	@RESULT="$(shell $(ROSIEBINSCRIPT) version 2> /dev/null)"; \
	EXPECTED="$(shell head -1 $(BUILD_ROOT)/VERSION)"; \
	if [ -n "$$RESULT" -a "$$RESULT" = "$$EXPECTED" ]; then \
	    echo "";\
            echo "Rosie Pattern Engine $$RESULT built successfully!"; \
	    if [ -z "$$BREW" ]; then \
	      	    echo "    Use 'make install' to install into DESTDIR=$(DESTDIR)"; \
	      	    echo "    Use 'make uninstall' to uninstall from DESTDIR=$(DESTDIR)"; \
	      	    echo "    To run rosie from the build directory, use: ./$(ROSIEBINSCRIPTNAME)"; \
	            echo "    Try this example, and look for color text output: ./$(ROSIEBINSCRIPTNAME) match all.things test/resolv.conf"; \
		    echo "";\
	    fi; \
            true; \
        else \
            echo "Rosie Pattern Engine test FAILED."; \
	    echo "    Rosie executable is $(ROSIEBIN)"; \
	    echo "    Expected this output: $$EXPECTED"; \
	    if [ -n "$$RESULT" ]; then \
		echo "    But received this output: $$RESULT"; \
	    else \
		echo "    But received no output to stdout."; \
	    fi; \
	    false; \
	fi

.PHONY: installcheck
installcheck: 
	@if [ ! -x $(INSTALL_ROSIEBIN) ]; then \
		echo "$(INSTALL_ROSIEBIN) does not exist or is not executable.  Run 'make install' to install rosie."; \
		false; \
	fi 
	@echo "Executable exists (\"$(INSTALL_ROSIEBIN)\")"
	@if [ ! -e "$(LIBROSIED)/$(LIBROSIE_DYLIB)" ]; then \
		echo "$(LIBROSIED)/$(LIBROSIE_DYLIB) does not exist.  Run 'make install' to install rosie."; \
		false; \
	fi 
	@$(INSTALL_ROSIEBIN) config >/dev/null 2>&1; \
	if [ $$? -ne 0 ]; then \
		echo "Test failed (perhaps a corrupt installation?)"; \
		false; \
	fi
	@home=`$(INSTALL_ROSIEBIN) config | $(INSTALL_ROSIEBIN) match -o subs 're.s* "ROSIE_HOME" "=" word.dq'`; \
	if [ $$? -ne 0 ]; then \
		echo "Test failed (perhaps an incomplete installation?)"; \
	else \
		if [ "$$home" == "\"$(ROSIED)\"" ]; then \
			echo "Installation test passed (rosie library found at $$home)"; \
		else \
			echo "Installation test failed (perhaps an incomplete installation?)"; \
		fi; \
	fi; \

# -----------------------------------------------------------------------------
# Tests need to be done with dumb terminal type because the cli and
# repl tests compare the expected output to the actual output byte by
# byte.  With other terminal types, the ANSI color codes emitted by
# Rosie can be munged by the terminal, making some tests fail when
# they should not.

# The test target can be used to test the rosie executable after
# installation, no matter where that is in the system:
#   make test ROSIE=/usr/local/bin/rosie
.PHONY: test
test:
	@echo "Path to rosie executable is $(ROSIE)"
	@$(BUILD_ROOT)/test/rosie-has-debug.sh $(ROSIE) 2>/dev/null; \
	if [ "$$?" -ne "0" ]; then \
	echo "Rosie was not built with LUADEBUG support.  Try 'make clean; make LUADEBUG=1'."; \
	exit -1; \
	fi;
	@echo Running tests in test/all.lua; \
	(TERM="dumb"; echo "ROSIE=\"$(ROSIE)\"; dofile \"$(BUILD_ROOT)/test/all.lua\"" | "$(ROSIE)" -D)

.PHONY: clean
clean: libclean
	-rm -rf "$(BUILD)" lib bin
	-$(MAKE) -C "$(LUA_DIR)" clean
	-$(MAKE) -C "$(JSON_DIR)" clean
	if [ -d "$(READLINE_DIR)" ]; then \
	  cd "$(READLINE_DIR)" && rm -f readline.so && rm -f src/lua_readline.o; \
	fi
	-rm -f build.log

.PHONY: libclean
libclean:
	$(MAKE) -C "$(RPEG_DIR)" clean
	$(MAKE) -C "$(LIBROSIE_DIR)" clean


