FROM elementary/docker:juno-stable
ARG branch

RUN apt-get -y update && apt-get install -y \
  gcc \
  git \
  libreadline-dev \
  make \
  readline-common \
  libbsd-dev

# This COPY is designed to trigger re-running the git clone when the repo changes.
COPY githead-$branch /opt/githead-$branch
RUN git clone --depth 1 --branch $branch https://gitlab.com/rosie-pattern-language/rosie.git /opt/rosie

WORKDIR /opt/rosie
RUN make LUADEBUG=1
RUN make test
RUN make clean; make
RUN make install

# A couple of small tests to ensure the installed copy functions:
RUN rosie version
RUN rosie match all.things test/resolv.conf

RUN uname -a
RUN cat /etc/os-release
