-- -*- Mode: Lua; -*-                                                               
--
-- expr.lua
--
-- © Copyright Jamie A. Jennings 2018.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHOR: Jamie A. Jennings

local expr = {}

local ast = import("ast")
-- local common = import("common")

function expr.cook(ast_in)
   return ast.ambient_cook_exp(ast_in)
end

function expr.apply(name, ast_in)
   local ref = ast.ref.new{localname=name}
   return ast.application.new{ref=ref,
			      arglist={ast_in},
			      sourceref=ast_in.sourceref}
end



return expr


